FROM debian:stable AS builder
ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get --assume-yes update
RUN apt-get install --assume-yes --no-install-recommends tzdata
RUN apt-get --assume-yes install curl build-essential cmake libpng-dev libjpeg-dev libgif-dev libtiff-dev

RUN curl https://storage.googleapis.com/downloads.webmproject.org/releases/webp/libwebp-1.2.0.tar.gz | tar xvz
RUN mkdir libwebp-1.2.0/build
WORKDIR /libwebp-1.2.0/build
RUN cmake -DWEBP_BUILD_CWEBP=ON -DWEBP_BUILD_DWEBP=ON ../
RUN make
RUN make install

FROM debian:stable-slim
RUN apt-get --assume-yes update
RUN apt-get install -y --no-install-recommends libpng-dev libjpeg-dev libgif-dev libtiff-dev
COPY --from=builder /usr/local/include/webp /usr/local/include
COPY --from=builder /usr/local/lib/libwebp.* /usr/local/lib
COPY --from=builder /usr/local/bin/cwebp /bin
COPY --from=builder /usr/local/bin/dwebp /bin